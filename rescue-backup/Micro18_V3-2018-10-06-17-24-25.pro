update=21.08.2018 23:57:04
version=1
last_client=kicad
[pcbnew]
version=1
LastNetListRead=
UseCmpFile=1
PadDrill=0.600000000000
PadDrillOvalY=0.600000000000
PadSizeH=1.500000000000
PadSizeV=1.500000000000
PcbTextSizeV=1.500000000000
PcbTextSizeH=1.500000000000
PcbTextThickness=0.300000000000
ModuleTextSizeV=1.000000000000
ModuleTextSizeH=1.000000000000
ModuleTextSizeThickness=0.150000000000
SolderMaskClearance=0.000000000000
SolderMaskMinWidth=0.000000000000
DrawSegmentWidth=0.200000000000
BoardOutlineThickness=0.100000000000
ModuleOutlineThickness=0.150000000000
[cvpcb]
version=1
NetIExt=net
[general]
version=1
[eeschema]
version=1
LibDir=
[eeschema/libraries]
LibName1=power
LibName2=device
LibName3=switches
LibName4=relays
LibName5=motors
LibName6=transistors
LibName7=conn
LibName8=linear
LibName9=regul
LibName10=74xx
LibName11=cmos4000
LibName12=adc-dac
LibName13=memory
LibName14=xilinx
LibName15=microcontrollers
LibName16=dsp
LibName17=microchip
LibName18=analog_switches
LibName19=motorola
LibName20=texas
LibName21=intel
LibName22=audio
LibName23=interface
LibName24=digital-audio
LibName25=philips
LibName26=display
LibName27=cypress
LibName28=siliconi
LibName29=opto
LibName30=atmel
LibName31=contrib
LibName32=valves
LibName33=1-179277-5
LibName34=1N4148WS-7-F
LibName35=1N5817
LibName36=74xgxx
LibName37=0805B105K160CT
LibName38=1591-7020
LibName39=3350-4275-126
LibName40=3350-4275-246
LibName41=06033C223KAT2A
LibName42=7090.9010.03
LibName43=68001-202HLF
LibName44=70553-0038
LibName45=150060RS75000
LibName46=744227
LibName47=ABLS-8.000MHZ-B4-T
LibName48=ABM7-8.000MHZ-D2Y-T
LibName49=ac-dc
LibName50=actel
LibName51=allegro
LibName52=Altera
LibName53=analog_devices
LibName54=battery_management
LibName55=bbd
LibName56=BC848B_235
LibName57=BLM18KG471SH1D
LibName58=bosch
LibName59=brooktre
LibName60=BSS83P_H6327
LibName61=C0402C103J5RACTU
LibName62=C0402C222K5RACTU
LibName63=C0402C223K4RACTU
LibName64=C0603C104K4RACTU
LibName65=C0603C200K4GAC7867
LibName66=C0805C106K8PACTU
LibName67=CBR06C101F5GAC
LibName68=CGA3E1X7R1C105K080AC
LibName69=CGA4J3X5R1A106K125AB
LibName70=cmos_ieee
LibName71=CRCW12061K00JNEAHP
LibName72=dc-dc
LibName73=diode
LibName74=EEEFK1V471SP
LibName75=elec-unifil
LibName76=ESD_Protection
LibName77=FC-135_32.7680KA-AC
LibName78=ftdi
LibName79=gennum
LibName80=graphic_symbols
LibName81=hc11
LibName82=HEF4027BT
LibName83=infineon
LibName84=intersil
LibName85=ir
LibName86=L78L05ACD13TR
LibName87=L78S05CV-DG
LibName88=L78S12CV
LibName89=Lattice
LibName90=leds
LibName91=LEM
LibName92=LM358AMX
LibName93=logic_programmable
LibName94=LTST-C191KGKT
LibName95=LTST-C191KRKT
LibName96=maxim
LibName97=MC-146_32.7680KA-A0_ROHS
LibName98=MCMR06X103_JTL
LibName99=MCP2551-I_SN
LibName100=MCSR04X1004FTL
LibName101=MCSR06X39R0FTL
LibName102=MCSR06X1003FTL
LibName103=mechanical
LibName104=MF50_100R
LibName105=microchip_dspic33dsc
LibName106=microchip_pic10mcu
LibName107=microchip_pic12mcu
LibName108=microchip_pic16mcu
LibName109=microchip_pic18mcu
LibName110=microchip_pic24mcu
LibName111=microchip_pic32mcu
LibName112=modules
LibName113=motor_drivers
LibName114=MPU-6050
LibName115=msp430
LibName116=MX23A18NF1
LibName117=MX34032NF2
LibName118=MX44006NF1
LibName119=N2540-6002RB
LibName120=nordicsemi
LibName121=nxp
LibName122=nxp_armmcu
LibName123=onsemi
LibName124=Oscillators
LibName125=Power_Management
LibName126=powerint
LibName127=pspice
LibName128=RC0402FR-071ML
LibName129=RC0603FR-075K6L
LibName130=RC0603JR-071KL
LibName131=RC0603JR-134K7L
LibName132=RC0603JR-0710KL
LibName133=RC0603JR-0722RL
LibName134=RC0603JR-07510RL
LibName135=references
LibName136=rfcom
LibName137=RFSolutions
LibName138=RT9193-33GB
LibName139=sensors
LibName140=silabs
LibName141=SN74AHCT1G08DBVR
LibName142=SN74LVC2G32DCUR
LibName143=stm8
LibName144=stm32
LibName145=STM32F103C8T6
LibName146=STM32F103C8T6TR
LibName147=supertex
LibName148=SZNUP2105LT3G
LibName149=transf
LibName150=triac_thyristor
LibName151=ttl_ieee
LibName152=video
LibName153=wiznet
LibName154=Worldsemi
LibName155=Xicor
LibName156=zetex
LibName157=Zilog
LibName158=RC0603FR-071ML
LibName159=C0603C102K1GECTU
LibName160=C0603C105Z8VACTU
LibName161=C0603C223K5RACTU
LibName162=RC0603FR-07120RL
LibName163=SML-D13DWT86A
LibName164=SML-D13FWT86
LibName165=C0603C569D5GACTU
LibName166=PE-1812ACC110STS
LibName167=MX34003NF1
LibName168=MX34005NF1
LibName169=RC0603JR-0768RL
LibName170=R-78E5.0-0.5
LibName171=RC0603JR-071ML
